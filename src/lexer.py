import ply.lex as lex

# list of tokens
tokens = ('OPERATE',
          'KIND',
          'MANUAL',
          'EXIT',
)

# Regular expression rules for simple tokens
def t_OPERATE(t):
    r'RUN|[Rr]un|OPEN|[Oo]pen|CLOSE|[Cc]lose|CONNECT|[Cc]onnect|SEARCH|[Ss]earch'
    return t

def t_MANUAL(t):
    r'MANUAL|[Mm]anual'
    return t

def t_EXIT(t):
    r'EXIT|[Ee]xit|QUIT|[Qq]uit|BYE|[Bb]ye|FAREWELL|[Ff]arewell|LATER|[Ll]ater'
    return t

def t_KIND(t):
    r'[a-zA-ZęóąśłżźćńĘÓĄŚŁŻŹĆŃ\-_\.0-9]+'
    return t

# Error handling rule
def t_error(t):
    print('Illegal character \'%s\'' % t.value[0])
    t.lexer.skip(1)

# A string containing ignored characters (spaces and tabs)
t_ignore = ' \t'

# Build the lexer
lexer = lex.lex()
