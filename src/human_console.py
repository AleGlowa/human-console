from lexer import tokens
import sys

if len(sys.argv) > 1:
    if sys.argv[1] == 'text':
        from parser import *
        conversation()

    elif sys.argv[1] == 'speech':
        from parser_speech import *
        conversation()

    else:
        print('Please, pass as an argument: \'speech\' or \'text\'')
else:
        print('Please, pass as an argument: \'speech\' or \'text\'')