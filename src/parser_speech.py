import speech_recognition as sr
from gtts import gTTS
import pyttsx3
import sys                  # for sys.exit()
import os                   # for paths search
import subprocess           # for opening documents, programs, sites
import psutil               # for killing processes
import ply.yacc as yacc     # parser
from lexer import tokens    # avaible tokens
import requests             # for checking if site exists
import re                   # for substitute strings

user = 'alex'
info_for_dummies = 'The list of all possible commands is displayed by entering command \'manual\'\n'

def p_command(p):
    '''command : program_operate
               | manual
               | exit'''
    p[0] = p[1]

def p_program_operate(p):
    'program_operate : OPERATE KIND'
    isExist = False
    if p[1] in ['Run', 'run', 'RUN', 'Open', 'open', 'OPEN']:
        if p[2].endswith(('.csv', '.dat', '.db', '.dbf', '.log', '.mdb', '.sav', '.sql', '.tar', '.xml', '.ods')):
            for root, _, files in os.walk('/home/' + user + '/Documents/'):
                for file in files:
                    if file == p[2]:
                        subprocess.call(('xdg-open', os.path.join(root, file)), shell=False)
                        isExist = True
        else:
            for program in os.listdir('/bin/'):
                if program == p[2]:
                    subprocess.Popen(os.path.join('/bin/', program), shell=False)
                    isExist = True
    elif p[1] in ['Close', 'close', 'CLOSE']:
        for proc in psutil.process_iter():
            if proc.name() == p[2]:
                proc.kill()
                isExist = True
    elif p[1] in ['Connect', 'connect', 'CONNECT']:
        if 'http://' not in p[2] and 'https://' not in p[2]:
            p[2] = 'https://' + p[2]
        if 'www.' not in p[2]:
            p[2] = p[2][:8] + 'www.' + p[2][8:]
        try:
            request = requests.get(p[2])
        except:
            p[2] = p[2].replace('https://', 'http://')
            site_name = re.sub(r'www\.|http:\/\/www\.|https:\/\/www\.', '', p[2])
            try:
                request = requests.get(p[2])
            except:
                tts = gTTS(text='%s doesn\'t exists' % p[2], lang='en')
                tts.save('../data/hello.mp3')
                os.system('mpg123 ../data/hello.mp3')
            else:
                p = subprocess.Popen(['opera', p[2]])
        else:
            p = subprocess.Popen(['opera', p[2]])
        isExist = True
    if not isExist:
        tts = gTTS(text='Can\'t find the file/program//website ' + site_name, lang='en')
        tts.save('../data/hello.mp3')
        os.system('mpg123 ../data/hello.mp3')
    print()

def search(p):
    p = p.replace(r'Search', '').replace('search', '').replace('SEARCH', '').replace(' ', '+').replace('%', '%25')
    google_browser = 'www.google.pl/search?q='
    proc = subprocess.Popen(['opera', google_browser + p])

def p_exit(p):
    'exit : EXIT'
    if p[1] not in ['Exit', 'exit', 'EXIT', 'Close', 'close', 'CLOSE', 'Quit', 'quit', 'QUIT']:
        tts = gTTS(text=p[1] + ' :)', lang='en')
        tts.save('../data/hello.mp3')
        os.system('mpg123 ../data/hello.mp3')
    else:
        tts = gTTS(text='Goodbye', lang='en')
        tts.save('../data/hello.mp3')
        os.system('mpg123 ../data/hello.mp3')
    sys.exit()

def p_manual(p):
    'manual : MANUAL'
    with open('../data/manual.txt', 'r') as man_file:
        for line in man_file:
            line = line.rstrip()
            print(line)
    print()

def p_error(p):
    tts = gTTS(text='Sorry. I can\'t do it for you :( ' + info_for_dummies, lang='en')
    tts.save('../data/hello.mp3')
    os.system('mpg123 ../data/hello.mp3')

# Build the parser
parser = yacc.yacc()

r = sr.Recognizer()

def conversation():
    tts = gTTS(text='Welcome to the human console(HC) 0.1v. ' + info_for_dummies, lang='en')
    tts.save('../data/hello.mp3')
    os.system('mpg123 ../data/hello.mp3')
    while True:
        tts = gTTS(text='What can I do for you?', lang='en')
        tts.save('../data/hello.mp3')
        os.system('mpg123 ../data/hello.mp3')
        #user_input = input('\nUSER: ')
        with sr.Microphone() as source:
            r.adjust_for_ambient_noise(source)
            audio = r.listen(source)
        try:
            audioTtext = r.recognize_google(audio)
            print(audioTtext)
        except sr.UnknownValueError:
            tts = gTTS(text='I don\'t understand you', lang='en')
            tts.save('../data/hello.mp3')
            os.system('mpg123 ../data/hello.mp3')
        print()
        if audioTtext[:6] in ['search', 'Search', 'SEARCH']: 
            search(audioTtext)
        else:
            result = parser.parse(audioTtext)
