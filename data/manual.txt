				     MANUAL
				  
Commands can be capitalized or uppercase.
Description of commands:
    1. open [program/document]    # args: program - name of program/document
       run [program/document]     # opening a program/document.
       
    2. close [program/browser]   # args: program - name of program/website				        # closing a program/website.

    3. connect [website]	  # args: website - address of website
				  # open a website.

    4. search [phrase]		  # search phrase on google browser.

    5. manual            	  # open this file.

    6. exit/quit/bye/farewell/later	# close HC.
